USE [master]
GO
/****** Object:  Database [LoginApplication]    Script Date: 2/9/2016 9:15:24 AM ******/
CREATE DATABASE [LoginApplication]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LoginApplication', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\LoginApplication.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LoginApplication_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\LoginApplication_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LoginApplication] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LoginApplication].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LoginApplication] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LoginApplication] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LoginApplication] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LoginApplication] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LoginApplication] SET ARITHABORT OFF 
GO
ALTER DATABASE [LoginApplication] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LoginApplication] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [LoginApplication] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LoginApplication] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LoginApplication] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LoginApplication] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LoginApplication] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LoginApplication] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LoginApplication] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LoginApplication] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LoginApplication] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LoginApplication] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LoginApplication] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LoginApplication] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LoginApplication] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LoginApplication] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LoginApplication] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LoginApplication] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LoginApplication] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LoginApplication] SET  MULTI_USER 
GO
ALTER DATABASE [LoginApplication] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LoginApplication] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LoginApplication] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LoginApplication] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [LoginApplication]
GO
/****** Object:  Table [dbo].[UserData]    Script Date: 2/9/2016 9:15:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](70) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](15) NULL,
	[Status] [char](1) NOT NULL,
 CONSTRAINT [PK_userDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A=Active,D=Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserData', @level2type=N'COLUMN',@level2name=N'Status'
GO
USE [master]
GO
ALTER DATABASE [LoginApplication] SET  READ_WRITE 
GO
