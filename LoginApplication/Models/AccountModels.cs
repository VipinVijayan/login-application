﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Linq;

namespace LoginApplication.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("Entities")
        {
        }


    }







    public class UserDataManager
    {
        public string ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Status { get; set; }


        public string LoginUser(string username, string password)
        {
            using (var ContextObj = new Entities())
            {
                var result = ContextObj.UserDatas.Where(e => e.Username == username && e.Password == password).FirstOrDefault();
                if (result != null)
                    return result.FirstName;
                else
                    return "Failed";
            }

        }

        public string RegisterUser(UserData UserObj)
        {
            using (var ContextObj = new Entities())
            {
                ContextObj.UserDatas.Add(UserObj);
                if (ContextObj.SaveChanges() > 0)
                    return "Success";
                else
                    return "Failed";
            }

        }
    }



}
