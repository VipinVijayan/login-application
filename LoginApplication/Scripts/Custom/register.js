﻿
function UserRegister() {
    //var LoginObj = {};
    if (Validate()) {

        $("#Register").closest("form").submit();
      
      

    }
}
function Validate() {

    var eles = $("#Register input[type='text']");
    var selects = $("#Register input[type='password']");

    var Error = true;
    for (var i = 0; i < eles.length; i++) {
        var id = $(eles[i]).attr("id");
        if ($("#" + id).val() == "") {

            $("#" + id).attr("style", "border:1px solid red!important");

            Error = false;
        }
        else {

            $("#" + id).attr("style", "border:1px solid #ededed!important");
        }
    }
    for (var i = 0; i < selects.length; i++) {
        var id = $(selects[i]).attr("id");
        if ($("#" + id).val() == "") {
            $("#" + id).attr("style", "border:1px solid red!important");
            Error = false;
        }
        else {
            $("#" + id).attr("style", "border:1px solid #ededed!important");
        }
    }
    if (!validateMail($("#email").val())) {
        $("#email").attr("style", "border:1px solid red!important");
        Error = false;
    }
    if ($("#password").val() != $("#ConfirmPassword").val()) {
        $("#password").attr("style", "border:1px solid red!important");
        $("#ConfirmPassword").attr("style", "border:1px solid red!important");
        Error = false;
    }
    return Error;
}
function validateMail(email) {
    var expr = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return expr.test(email);
}