﻿
function UserLogin() {
    //var LoginObj = {};
    if (Validate()) {

        $("#LoginForm").closest("form").submit();
      
      

    }
}
function Validate() {

    var eles = $("#LoginForm input[type='text']");
    var selects = $("#LoginForm input[type='password']");

    var Error = true;
    for (var i = 0; i < eles.length; i++) {
        var id = $(eles[i]).attr("id");
        if ($("#" + id).val() == "") {

            $("#" + id).attr("style", "border:1px solid red!important");

            Error = false;
        }
        else {

            $("#" + id).attr("style", "border:1px solid #ededed!important");
        }
    }
    for (var i = 0; i < selects.length; i++) {
        var id = $(selects[i]).attr("id");
        if ($("#" + id).val() == "") {
            $("#" + id).attr("style", "border:1px solid red!important");
            Error = false;
        }
        else {
            $("#" + id).attr("style", "border:1px solid #ededed!important");
        }
    }

    return Error;
}