﻿using LoginApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoginApplication.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View(new UserDataManager());
        }
        [HttpPost]
        public ActionResult Index(UserDataManager obj)
        {
            UserDataManager LoginManagerObj = new UserDataManager();
            string result = LoginManagerObj.LoginUser(obj.Username, obj.Password);
            if (result == "Failed")
            {
                TempData["Message"] = "Username or password not matched";
            }
            else
            {
                TempData["Message"] = result + " Welcome To GL InfoTech";


                return RedirectToAction("/LoginedSuccess");
            }
            return View();
        }
        public ActionResult LoginedSuccess()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View(new UserDataManager());
        }
        [HttpPost]
        public ActionResult Register(UserDataManager obj)
        {
            UserDataManager RegisterManagerObj = new UserDataManager();

            UserData userObj = new UserData();
            userObj.Email = obj.Email;
            userObj.FirstName = obj.FirstName;
            userObj.LastName = obj.LastName;
            userObj.Password = obj.Password;
            userObj.PhoneNumber = obj.PhoneNumber;
            userObj.Username = obj.Email;
            userObj.Status = "A";

            string result = RegisterManagerObj.RegisterUser(userObj);
            if (result != "Success")
            {
                ViewData["Message"] = "Something happened on Registration , Please try again later";
            }
            else
            {
                TempData["Message"] = obj.FirstName + " Welcome To GL InfoTech";
                return RedirectToAction("/LoginedSuccess");
            }
            return View();
        }
    }
}
