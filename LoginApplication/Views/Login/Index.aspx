﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LoginApplication.Models.UserDataManager>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
 Gl Infotech - Login
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("Index", "Login", FormMethod.Post, new { @id = "LoginForm" }))
       { %>
    <%: Html.ValidationSummary(true) %>


    <div class="sign_in_left_main">
        <div class="sign_in_left_container" id="sign_in">
            <h1>Sign In</h1>
            <div class="sign_in_form_wrap">
                <form>
                    <%: Html.TextBoxFor(model => model.Username,new { @class = "sign_in_txt_field" ,id="Username",placeholder="Enter Email (username)"}) %>
                    <%: Html.PasswordFor(model => model.Password,new { @class = "sign_in_txt_field" ,id="Password",placeholder="Enter Password"}) %>
                    <%if (ViewData["Message"] != null)
                      { %>
                    <div class="sign_in_forgot_password"><%:ViewData["Message"] %></div>
                    <%} %>
                    <input type="button" class="sign_in_submit" value="LOGIN" onclick="UserLogin()" />
                </form>
                   <%--<div class="sign_in_forgot_password"><a href="javascript:void(0)" class="trigger_forgot">Forgot Your Password ?</a></div>--%>
            </div>
            <!--sign_in_form_wrap-->
            <div class="dont_have_ac_wrap">Don't have an account? <span><a href="Login/Register">Create one</a></span></div>
        </div>
        <!--sign_in_left_container-->

        <div class="sign_in_left_container" id="forgot_password">
            <h1>Forgot Password</h1>
            <div class="sign_in_form_wrap">
                <form>
                    <input type="text" class="sign_in_txt_field" placeholder="Enter Mobile Number or Email">

                    <button class="sign_in_submit">SUBMIT</button>
                </form>
                <div class="sign_in_forgot_password"><a href="javascript:void(0)" class="trigger_return_login">Return To Login</a></div>
            </div>
            <!--sign_in_form_wrap-->
            <div class="dont_have_ac_wrap">Don't have an account? <span><a href="/Register">Create one</a></span></div>
        </div>
        <!--sign_in_left_container-->


    </div>
    <!--sign_in_left_main-->

    <div class="sign_in_right_main">
        <div class="sign_in_right_main_container">
            <img src="images/gl_logo.png" />
        </div>
    </div>
    <!--sign_in_right_main-->

    <%--<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>LoginModel</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Username) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Username) %>
            <%: Html.ValidationMessageFor(model => model.Username) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ConfirmPassword) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ConfirmPassword) %>
            <%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Status) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Status) %>
            <%: Html.ValidationMessageFor(model => model.Status) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>--%>
    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopScriptSection" runat="server">

    <%: Styles.Render("~/Content/loginStyles") %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">

    <%: Scripts.Render("~/Scripts/LoginScripts") %>
</asp:Content>
