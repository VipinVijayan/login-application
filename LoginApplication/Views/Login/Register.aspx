﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LoginApplication.Models.UserDataManager>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  Gl Infotech - Register User
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



    <% using (Html.BeginForm("Register", "Login", FormMethod.Post, new { @id = "Register" }))
       { %>
    <%: Html.ValidationSummary(true)%>


    <div class="vendor_register_left_main">
        <div class="vendor_register_left_container">
            <h1>Customer Register</h1>
            <div class="customer_register_form_wrap">

                <%: Html.TextBoxFor(model => model.FirstName, new { @class = "vendor_register_txt_field", id = "firstname", placeholder = "First Name" })%>
                <%: Html.TextBoxFor(model => model.LastName, new { @class = "vendor_register_txt_field", id = "lastname", placeholder = "Last Name" })%>
                <%: Html.TextBoxFor(model => model.Email, new { @class = "vendor_register_txt_field", id = "email", placeholder = "Email" })%>
                <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "vendor_register_txt_field", id = "phonenumber", placeholder = "Phone Number" })%>
                <%: Html.PasswordFor(model => model.Password, new { @class = "vendor_register_txt_field", id = "password", placeholder = "Enter Password" })%>
                <%: Html.PasswordFor(model => model.ConfirmPassword, new { @class = "vendor_register_txt_field", id = "ConfirmPassword", placeholder = "Confirm Password" })
                %>
                <%if (ViewData["Message"] != null)
                  { %>
                <div class="sign_in_forgot_password"><%:ViewData["Message"]%></div>
                <%}
                %>
                <input type="button" value="REGISTER" class="sign_in_submit" onclick="UserRegister()"/>


            </div>
            <!--customer_register_form_wrap-->
            <!--<div class="already_have_ac_wrap">Already have an account? <span><a href="#">Sign In</a></span></div>-->
        </div>
        <!--vendor_register_left_container-->
    </div>
    <!--vendor_register_left_main-->


    <%} %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopScriptSection" runat="server">
     <%: Styles.Render("~/Content/loginStyles") %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/Scripts/RegisterScripts") %>
</asp:Content>
