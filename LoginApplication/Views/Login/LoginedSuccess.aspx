﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LoginApplication.Models.UserDataManager>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Logined Successfully
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="sign_in_left_main">
        <div class="sign_in_left_container" id="sign_in">




            <!--sign_in_left_container-->
            <%if (TempData["Message"] != null)
              { %>
            <h1><%:TempData["Message"]%></h1>
            <%}
            %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopScriptSection" runat="server">
         <%: Styles.Render("~/Content/loginStyles") %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
